Notes for a demo about incus (ne LXD) Linux container and virtual
machine manager.

# Yet another container manager

We already have docker and podman and the whole ecosystem that
includes kubernetes.

We also have openstack and ovirt virtualization projects

Why do we need another container and virtual machine manager?

# Incus

"Incus is a next generation system container and virtual machine
manager."

## Docker vs Incus
yb
- Docker (and podman) are "application" centric container managers
- Incus is a "system" centric container and vm manager.

| | Cntainers | Virtual Macines | Clustering | Storage | Networking |
---------------------------------------------
| Docker | X | | |
| Kubernetes | | | X |
| Incus | X | X | X | X | X | 

# Three Systems

- First system
  - poc, 
  - limited features, Maybe not the right ones
  - "MVP"
- Second System
  - Overly feature rich
  - Complex, Sometimes suffers from inner platform 
  - Often becomes entrenched and instant legacy.
- Third system
  - We pretty much know what we want
  - Just the needed features
  - "As simple as possible but no simpler."

Incus is pretty close to a third system. It has the features we want
but not many we don't want.



# Demo

- Two Raspbery Pi 4
- Cluster LVM store
- Local store
- Container mobility
- VM mobility
